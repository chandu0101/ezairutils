package com.chandu0101.ezair;

import com.airhacks.afterburner.injection.InjectionProvider;
import com.chandu0101.ezair.presentation.base.BasePresenter;
import com.chandu0101.ezair.presentation.base.BaseView;
import com.chandu0101.ezair.service.PersistanceFactory;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author chandu0101
 */
public class App extends Application {

    private static  App instance;
    private Stage stage;

    public Stage getStage() {
        return this.stage;
    }

    public static App getInstance() {
        return  instance;
    }

    public  App () {
       instance = this;
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        PersistanceFactory.createEntityManagerFactory();
        BaseView appView = new BaseView();
        Scene scene = new Scene(appView.getView());
        BasePresenter appController = (BasePresenter) appView.getPresenter();
        appController.setStageRef(stage);
        stage.setTitle("EzairUtils.fx v1.0-SNAPSHOT");
        final String uri = getClass().getResource("app.css").toExternalForm();
        scene.getStylesheets().add(uri);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        InjectionProvider.forgetAll();
        PersistanceFactory.closeEMF();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
