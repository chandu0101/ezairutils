package com.chandu0101.ezair.customcomponent.searchbox;

/**
 * Created by chandrasekharkode on 12/23/13.
 */
public interface Searchable {

    public String getValue();
}
