/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.util;

import com.chandu0101.ezair.presentation.websphere.WebspherePresenter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import javafx.scene.control.TextArea;
import static com.chandu0101.ezair.constants.EzairConstants.*;
import java.io.IOException;

/**
 *
 * @author chandrasekharkode
 */
public class FileUtils {

    public final static Charset ENCODING = StandardCharsets.UTF_8;
    private final static TextArea console = WebspherePresenter.getInstance().getWebsphereConsole();
    
    public static Properties readProperties(String filePath) {
        Properties properties = null;
        Path path = Paths.get(filePath);
        try (BufferedReader reader = Files.newBufferedReader(path, ENCODING)) {
            properties = new Properties();
            properties.load(reader);
        } catch (Exception ex) {
            CommandPrompt.write("Exception occured while reading properties file :"+ filePath + ":=" +ex.getMessage(),console);
            
        }
        
        return properties;
    }
    
    public static String writeProperties(Properties properties,String filePath) {
        Path path = Paths.get(filePath);
        String result = SUCCESS;
        try(BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
            properties.store(writer, null);
        } catch (Exception ex) {
            CommandPrompt.write("Exception occured while writing to properties file := "+ filePath + ":=" +ex.getMessage(),console);
        }
        return result;
    }
    
    public static void writeToFile(String data,Path path) {
        try(BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
            CommandPrompt.write("Writing Content to File", console);
            writer.write(data);
            CommandPrompt.write("File Updated Successfully", console);
        } catch (IOException ex) {
            CommandPrompt.write("Exception occured while writing to file := "+ path + ":=" +ex.getMessage(),console);
        }
    }
}
