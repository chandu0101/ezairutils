/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chandu0101.ezair.util;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static com.chandu0101.ezair.constants.EzairConstants.*;
/**
 *
 * @author chandrasekharkode
 */
public class CommandPrompt {

    public static String execute(String command, TextArea console){
        String result = SUCCESS;
        write("Executing Command := "+ command, console);
        try {
           ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
           builder.redirectErrorStream(true);
           Process p = builder.start();
           p.waitFor();
           BufferedReader reader = new BufferedReader(new InputStreamReader(
                   p.getInputStream()));
           String line = reader.readLine();
           write(line, console);
           while (line != null) {
               line = reader.readLine();
               write(line, console);      
           }   
        } catch (Exception e) {
            result = ERROR;
            write("Exception while executing command := "+command +">>"+e.getMessage(), console);
        }
       return result;
    }

    public static void write(final String text, TextArea console){
        if(StringUtils.isNotEmpty(text)) {
            Platform.runLater(() -> {
                console.appendText(text + NEW_LINE);
            }); 
      }
    }
}
