package com.chandu0101.ezair.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by chandrasekharkode on 1/23/14.
 */
public  class DateUtil {

    private DateUtil() {
    }

   public static LocalDate getLocalDate(Date date) {
        LocalDate result = null;
        if(date != null) {
            Instant instant = Instant.ofEpochMilli(date.getTime());
            result = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        }
        return result;
    }

    public static boolean isBetween(LocalDate date, LocalDate starDate,LocalDate endDate) {
        return !date.isAfter(endDate) && !date.isBefore(starDate);
    }

    public static LocalDate convertStringToLocalDate(String dateString, String format) {
        return  LocalDate.parse(dateString, DateTimeFormatter.ofPattern(format));
    }
}
