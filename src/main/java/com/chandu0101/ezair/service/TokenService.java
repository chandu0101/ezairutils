package com.chandu0101.ezair.service;

import com.chandu0101.ezair.entity.Token;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

/**
 * Created by chandrasekharkode on 12/10/13.
 */
public class TokenService {

    private EntityManager em;
    private EntityTransaction et;

    @PostConstruct
    public void init() {
        this.em = PersistanceFactory.createEntityManager();
        this.et = this.em.getTransaction();
    }

    public List<Token> getAll() {
        return em.createNamedQuery(Token.findAll).getResultList();
    }

    public Token save(Token token) {
        et.begin();
        Token savedToken = em.merge(token);
        et.commit();
        return  savedToken;
    }

    public void remove(Token token) {
        et.begin();
        em.remove(token);
        et.commit();
    }
}
