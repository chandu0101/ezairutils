/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.service;

import com.chandu0101.ezair.entity.DocRoot;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author chandrasekharkode
 */
public class DocRootService {
    
    private EntityManager em;
    private EntityTransaction et;
      
    @PostConstruct
    public void init() {
        this.em = PersistanceFactory.createEntityManager();
        this.et = this.em.getTransaction();
    }
    
    public DocRoot save(DocRoot docRoot) {
        this.et.begin();
        DocRoot saved = this.em.merge(docRoot);
        this.et.commit();
        return saved; 
    }
    
    public List<DocRoot> getAll() {  
        return this.em.createNamedQuery(DocRoot.FINDALL).getResultList();
    }
    
    public void remove(DocRoot docRoot) {
        this.et.begin();
        this.em.remove(docRoot);
        this.et.commit();
    }
}
