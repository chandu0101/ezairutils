/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author chandrasekharkode
 */
public class PersistanceFactory {
    
    private static EntityManagerFactory emf ;
    
    private PersistanceFactory(){
    }
    
    public static void createEntityManagerFactory() {
        if(emf == null) {
             emf = Persistence.createEntityManagerFactory("ezairutils");
        }
    }    
    public static EntityManager  createEntityManager() {
        return emf.createEntityManager();
    }
    
    public static void closeEMF() {
        if(emf != null && emf.isOpen()) {
            emf.close();
        }
        emf = null;
    }
     
}
