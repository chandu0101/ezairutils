/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.entity;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author chandrasekharkode
 */

@Entity
@NamedQueries({
    @NamedQuery(name = DocRoot.FINDALL ,query = "Select d from DocRoot d")
})
public class DocRoot implements Serializable {
    
    public static final String PREFIX = "com.chandu0101.ezair.entity.DocRoot.";
    public static final String FINDALL = PREFIX+"findall";
    @Id
    private String id;
    private String docRootPath;

    
    public DocRoot() {
        this.id = UUID.randomUUID().toString();
    }
    
    public DocRoot(String path){
        this();
        this.docRootPath = path;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocRootPath() {
        return docRootPath;
    }

    public void setDocRootPath(String docRootPath) {
        this.docRootPath = docRootPath;
    }

    
    
}
