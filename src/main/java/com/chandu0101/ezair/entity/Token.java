package com.chandu0101.ezair.entity;

import com.chandu0101.ezair.customcomponent.searchbox.Searchable;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;

/**
 * Created by chandrasekharkode on 12/6/13.
 */

@Entity
@NamedQueries({
        @NamedQuery(name = Token.findAll, query = "SELECT t from Token t")
})
public class Token implements Searchable {

    public final static String PREFIX = "com.chandu0101.ezair.entity.Token.";
    public final static String findAll = PREFIX + "findAll";
    private static final String SPACE = " ";

    private SimpleLongProperty idProperty;
    private StringProperty tokenProperty;
    private  StringProperty productCompanyProperty;
    private  StringProperty bookingCompanyProperty;
    private  StringProperty environmentProperty;
    private  StringProperty localLinkProperty;
    private  StringProperty enviLinkProperty;
    private StringProperty commentProperty;

    public Token(){
        idProperty = new SimpleLongProperty();
        tokenProperty = new SimpleStringProperty();
        productCompanyProperty = new SimpleStringProperty();
        bookingCompanyProperty = new SimpleStringProperty();
        environmentProperty = new SimpleStringProperty();
        localLinkProperty = new SimpleStringProperty();
        enviLinkProperty = new SimpleStringProperty();
        commentProperty = new SimpleStringProperty();
    }

    public String getToken() {
        return tokenProperty.get();
    }

    public StringProperty tokenProperty() {
        return tokenProperty;
    }

    public void setToken(String token) {
        this.tokenProperty.set(token);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return idProperty.get();
    }

    public SimpleLongProperty idProperty() {
        return idProperty;
    }

    public void setId(Long id) {
        this.idProperty.set(id);
    }

    public String getProductCompany() {
        return productCompanyProperty.get();
    }

    public StringProperty productCompanyProperty() {
        return productCompanyProperty;
    }

    public void setProductCompany(String productCompany) {
        this.productCompanyProperty.set(productCompany);
    }

    public String getBookingCompany() {
        return bookingCompanyProperty.get();
    }

    public StringProperty bookingCompanyProperty() {
        return bookingCompanyProperty;
    }

    public void setBookingCompany(String bookingCompany) {
        this.bookingCompanyProperty.set(bookingCompany);
    }

    public String getEnvironment() {
        return environmentProperty.get();
    }

    public StringProperty environmentProperty() {
        return environmentProperty;
    }

    public void setEnvironment(String environment) {
        this.environmentProperty.set(environment);
    }

    public String getLocalLink() {
        return localLinkProperty.get();
    }

    public StringProperty localLinkProperty() {
        return localLinkProperty;
    }

    public void setLocalLink(String localLink) {
        this.localLinkProperty.set(localLink);
    }

    public String getEnviLink() {
        return enviLinkProperty.get();
    }

    public StringProperty enviLinkProperty() {
        return enviLinkProperty;
    }

    public void setEnviLink(String enviLink) {
        this.enviLinkProperty.set(enviLink);
    }

    public String getComment() {
        return commentProperty.get();
    }

    public StringProperty commentProperty() {
        return commentProperty;
    }

    public void setComment(String comment) {
        this.commentProperty.set(comment);
    }

    @Override
    @Transient
    public String getValue() {
        return getId() + SPACE +getToken() +SPACE+getProductCompany()+SPACE+getBookingCompany()+SPACE+getEnvironment()+SPACE+getEnviLink()+SPACE+getLocalLink()+SPACE+getComment();
    }
}
