package com.chandu0101.ezair.presentation.Token;

import com.chandu0101.ezair.App;
import com.chandu0101.ezair.constants.BookingCompany;
import com.chandu0101.ezair.constants.PolarEnvironment;
import com.chandu0101.ezair.constants.ProductCompany;
import com.chandu0101.ezair.customcomponent.searchbox.SimpleSearchBox;
import com.chandu0101.ezair.entity.Token;
import com.chandu0101.ezair.presentation.Token.add.AddPresenter;
import com.chandu0101.ezair.presentation.Token.add.AddView;
import com.chandu0101.ezair.service.TokenService;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.controlsfx.control.PopOver;
import org.controlsfx.dialog.Dialogs;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by chandrasekharkode on 12/6/13.
 */
public class TokenPresenter implements Initializable {


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;


    private TableColumn<Token, String> bookingCompanyColumn;


    private TableColumn<Token, String> commentColumn;


    private TableColumn<Token, String> enviLinkColumn;


    private TableColumn<Token, String> environmentColumn;


    private TableColumn<Token, Integer> idColumn;

    private TableColumn<Token, String> localLinkColumn;


    private TableColumn<Token, String> productCompanyColumn;

    private TableColumn<Token, String> tokenColumn;

    @FXML
    private TableView<Token> tokensTableView;

    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;

    @FXML
    private Button editButton;

    @FXML
    private SimpleSearchBox<Token> searchBox;

    @Inject
   private TokenService tokenService;

    private ObservableList<Token> allTokens;

    private FilteredList<Token> filteredTokens;

    private PopOver addItemPopOver;
    private Stage stageRef;
    private AddPresenter addPresenter;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        prepareTable();
        AddView addView = new AddView();
        addPresenter = (AddPresenter) addView.getPresenter();
        this.allTokens = addPresenter.getAllTokens();
        filteredTokens = new FilteredList<>(allTokens);
        searchBox.setFilteredList(filteredTokens);
        tokensTableView.setItems(this.filteredTokens);
       new Thread(() ->  {
          this.allTokens.addAll(tokenService.getAll());
        }).start();
        addItemPopOver = new PopOver(addView.getView());
        addItemPopOver.setDetachedTitle("Add New Token");
        addPresenter.setAddItemPopOver(this.addItemPopOver);
        this.stageRef = App.getInstance().getStage();


    }

    private void prepareTable() {
        idColumn = createIntegerColumn("id","Id");
        tokenColumn = createStringColumn("token","Token");
        productCompanyColumn = createStringColumn("productCompany","PC");
        bookingCompanyColumn = createStringColumn("bookingCompany","BC");
        environmentColumn = createStringColumn("environment","Environment");
        localLinkColumn = createStringColumn("localLink","Local Link");
        localLinkColumn.setCellFactory(tc -> new ButtonCell());
        enviLinkColumn = createStringColumn("enviLink","Envi Link");
        enviLinkColumn.setCellFactory(tc -> new ButtonCell());
        enviLinkColumn.setCellFactory(new Callback<TableColumn<Token, String>, TableCell<Token, String>>() {
            @Override
            public TableCell<Token, String> call(TableColumn<Token, String> tokenStringTableColumn) {
                return new ButtonCell();
            }
        });
        commentColumn = createStringColumn("comment","Comment");
        tokensTableView.getColumns().addAll(idColumn,tokenColumn,productCompanyColumn,bookingCompanyColumn,environmentColumn,enviLinkColumn,localLinkColumn,commentColumn);
        tokensTableView.setEditable(true);
        tokensTableView.getSelectionModel().setCellSelectionEnabled(true);
        tokensTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    private TableColumn<Token,String> createStringColumn(String name,String title) {
        TableColumn tableColumn = new TableColumn(title);
        tableColumn.setCellValueFactory(new PropertyValueFactory<Token,String>(name));
        return tableColumn;
    }

    private TableColumn<Token,Integer> createIntegerColumn(String name,String title) {
        TableColumn tableColumn = new TableColumn(title);
        tableColumn.setCellValueFactory(new PropertyValueFactory<Token,Integer>(name));
        return tableColumn;
    }



    @FXML
    void handleAddToken(ActionEvent event) {
        addPresenter.resetFields();
        addPresenter.getSaveButton().setText("Save");
        addItemPopOver.show(addButton,stageRef.getX()+addButton.getWidth()+25,stageRef.getY()+addButton.getWidth()+75);
    }

    @FXML
    void handleDeleteToken(ActionEvent event) {
        Token token = tokensTableView.getSelectionModel().getSelectedItem();
        if(token != null) {
           tokenService.remove(token);
            allTokens.remove(token);
        } else {
            Dialogs.create()
                    .title("Delete Token")
                    .message("Please Select a row/cell in table before Deleting.").showInformation();
        }
    }

    @FXML
    void handleEditToken(ActionEvent event) {

        Token token = tokensTableView.getSelectionModel().getSelectedItem();
        if(token != null) {
          addPresenter.setSelectedToken(token);
          addPresenter.getSaveButton().setText("Update");
          addPresenter.getTokenTextField().setText(token.getToken());
          addPresenter.getProductCompanyChoiceBox().setValue(ProductCompany.valueOf(token.getProductCompany()));
          addPresenter.getBookingCompanyChoiceBox().setValue(BookingCompany.valueOf(token.getBookingCompany()));
          addPresenter.getEnvironmentChoiceBox().setValue(PolarEnvironment.valueOf(token.getEnvironment()));
          addPresenter.getCommentTextField().setText(token.getComment());
          addItemPopOver.show(addButton,stageRef.getX()+addButton.getWidth()+editButton.getWidth()+25,stageRef.getY()+addButton.getWidth()+75);

        } else {
            Dialogs.create()
                    .title("Edit Token")
                    .message("Please Select a row/cell in table before editing.").showInformation();
        }


    }


}
