package com.chandu0101.ezair.presentation.Token.add;

import com.chandu0101.ezair.constants.BookingCompany;
import com.chandu0101.ezair.constants.PolarEnvironment;
import com.chandu0101.ezair.constants.ProductCompany;
import com.chandu0101.ezair.entity.Token;
import com.chandu0101.ezair.service.TokenService;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.controlsfx.control.PopOver;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by chandrasekharkode on 12/8/13.
 */
public class AddPresenter implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ChoiceBox<BookingCompany> bookingCompanyChoiceBox;

    @FXML
    private TextField commentTextField;

    @FXML
    private ChoiceBox<PolarEnvironment> environmentChoiceBox ;

    @FXML
    private ChoiceBox<ProductCompany> productCompanyChoiceBox;

    @FXML
    private TextField tokenTextField;

    @FXML
    private Button saveButton;

    @Inject
    private TokenService tokenService;

    private ObservableList<Token> allTokens = FXCollections.observableArrayList();

    private PopOver addItemPopOver;

    public PopOver getAddItemPopOver() {
        return addItemPopOver;
    }

    public void setAddItemPopOver(PopOver addItemPopOver) {
        this.addItemPopOver = addItemPopOver;
    }

    public ObservableList<Token> getAllTokens() {
        return allTokens;
    }

    private Token selectedToken;

    public Button getSaveButton() {
        return saveButton;
    }

    public ChoiceBox<BookingCompany> getBookingCompanyChoiceBox() {
        return bookingCompanyChoiceBox;
    }

    public TextField getCommentTextField() {
        return commentTextField;
    }

    public ChoiceBox<PolarEnvironment> getEnvironmentChoiceBox() {
        return environmentChoiceBox;
    }

    public ChoiceBox<ProductCompany> getProductCompanyChoiceBox() {
        return productCompanyChoiceBox;
    }

    public TextField getTokenTextField() {
        return tokenTextField;
    }

    public Token getSelectedToken() {
        return selectedToken;
    }

    public void setSelectedToken(Token selectedToken) {
        this.selectedToken = selectedToken;
    }

    @FXML
    void handleCancel(ActionEvent event) {
        this.addItemPopOver.hide();
    }

    @FXML
    void handleSave(ActionEvent event) {
        if(saveButton.getText().equals("Save")) {
            allTokens.add(save(null));
        } else if(saveButton.getText().equals("Update")) {
            save(selectedToken);
        }
        this.addItemPopOver.hide();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        BooleanProperty tokenEntered = new SimpleBooleanProperty();
        tokenEntered.bind(tokenTextField.textProperty().length().isEqualTo(6));

        BooleanProperty pcEntered = new SimpleBooleanProperty();
        pcEntered.bind(productCompanyChoiceBox.valueProperty().isNotNull());

        BooleanProperty bcEntered = new SimpleBooleanProperty();
        bcEntered.bind(bookingCompanyChoiceBox.valueProperty().isNotNull());

        BooleanProperty environmentEntered = new SimpleBooleanProperty();
        environmentEntered.bind(environmentChoiceBox.valueProperty().isNotNull());
        environmentChoiceBox.setItems(FXCollections.observableArrayList(PolarEnvironment.values()));
        bookingCompanyChoiceBox.setItems(FXCollections.observableArrayList(BookingCompany.values()));
        productCompanyChoiceBox.setItems(FXCollections.observableArrayList(ProductCompany.values()));
        saveButton.disableProperty().bind(tokenEntered.and(pcEntered).and(bcEntered).and(environmentEntered).not());
    }


    public void resetFields() {
        tokenTextField.setText("");
        bookingCompanyChoiceBox.setValue(null);
        productCompanyChoiceBox.setValue(null);
        environmentChoiceBox.setValue(null);
        commentTextField.setText("");
    }

    private Token  save(Token token) {
        if(token == null) {
            token = new Token();
        }
        token.setToken(tokenTextField.getText());
        token.setBookingCompany(bookingCompanyChoiceBox.getValue().toString());
        token.setProductCompany(productCompanyChoiceBox.getValue().toString());
        token.setEnvironment(environmentChoiceBox.getValue().toString());
        prepareLinks(bookingCompanyChoiceBox.getValue(), productCompanyChoiceBox.getValue(), token);
        token.setComment(commentTextField.getText());
        return  tokenService.save(token);
    }

    private void prepareLinks(BookingCompany bookingCompany, ProductCompany productCompany,Token token) {
        String webToken = null;
        switch (productCompany) {

            case PC:
                     webToken = "IGTRRXP"+tokenTextField.getText();
                     createEnvironmentLink(webToken,token);
                     createLocalLink(webToken,token);
                     break;
            case HA:
                    webToken = "IGTRRXH"+tokenTextField.getText();
                    createEnvironmentLink(webToken,token);
                    createLocalLink(webToken,token);
                    break;
            case SB:
                    webToken = "IGTRRXB"+tokenTextField.getText();
                    createEnvironmentLink(webToken,token);
                    createLocalLink(webToken,token);
                    break;


        }


    }

    private void createLocalLink(String webToken, Token token) {
        token.setLocalLink("http://localhost/air/book.do?company="+bookingCompanyChoiceBox.getValue().toString()+"&token="+webToken);
    }

    private void createEnvironmentLink(String webToken, Token token) {
        switch (environmentChoiceBox.getValue()) {

            case RE1: case RE3:
                token.setEnviLink("https://dev1ezbook.princess.com/air/book.do?company=" + bookingCompanyChoiceBox.getValue().toString() + "&token=" +webToken);
                break;

            case RES :
                token.setEnviLink("https://devezbook.princess.com/air/book.do?company=" + bookingCompanyChoiceBox.getValue().toString() + "&token=" +webToken);
                break;

            case HAQ: case PCQ:
                token.setEnviLink("https://stageezbook.princess.com/air/book.do?company=" + bookingCompanyChoiceBox.getValue().toString() + "&token=" +webToken);
                break;

        }
    }
}
