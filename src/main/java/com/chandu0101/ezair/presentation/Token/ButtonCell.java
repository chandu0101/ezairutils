package com.chandu0101.ezair.presentation.Token;

import com.chandu0101.ezair.App;
import com.chandu0101.ezair.entity.Token;
import com.chandu0101.ezair.util.StringUtils;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;

/**
 * Created by chandrasekharkode on 12/11/13.
 */
public class ButtonCell extends TableCell<Token,String> {

    Button cellButton = new Button("Open");
    private String link;

    public ButtonCell () {
        cellButton.setOnAction( e -> {
            App.getInstance().getHostServices().showDocument(link);
        });
    }

    @Override
    protected void updateItem(String value, boolean empty) {
        super.updateItem(value, empty);
        if(!empty && StringUtils.isNotEmpty(value)) {
            setGraphic(cellButton);
            link = value;
        } else {
            setText(null);
            setGraphic(null);
        }
    }


}
