/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.presentation.websphere.docroot;

import com.chandu0101.ezair.presentation.websphere.WebspherePresenter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import static com.chandu0101.ezair.constants.EzairConstants.*;
import com.chandu0101.ezair.entity.DocRoot;
import com.chandu0101.ezair.service.DocRootService;
import com.chandu0101.ezair.util.CommandPrompt;
import com.chandu0101.ezair.util.FileUtils;
import com.chandu0101.ezair.util.StringUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javax.inject.Inject;

/**
 *
 * @author chandu0101
 */
public class DocRootPresenter implements Initializable{

    @FXML
    private TextField docRootPathTextField;

    @FXML
    private TextField webServerPathTextField;
    @FXML
    private ChoiceBox<String> previousPathsChiceBox;

    private TextArea console;
    
    @Inject
    private DocRootService docRootService;
    
    private ObservableList<String> allPaths;
    
    
            
    @FXML
    void changeDocRoot(ActionEvent event) {
     console.clear();
     if(validatePaths()) {
        new Thread(()-> {
        if (readAndUpdateFile(webServerPathTextField.getText()+"\\conf\\httpd.conf")) {
             CommandPrompt.write("Restarting Webserver", console);
             String command = "cd"+SPACE+webServerPathTextField.getText()+"\\bin"+SPACE+"&&"+SPACE+"httpd -k restart";
             if(SUCCESS.equals(CommandPrompt.execute(command, console))) {
                 CommandPrompt.write("Webserver Restarted successfully!", console);
             }
          } 
        }).start();

       if(!allPaths.contains(docRootPathTextField.getText())) {
          allPaths.add(docRootPathTextField.getText());
          docRootService.save(new DocRoot(docRootPathTextField.getText()));
         }
     } 
        
  }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        console = WebspherePresenter.getInstance().getWebsphereConsole();
        previousPathsChiceBox.valueProperty().addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
           docRootPathTextField.setText(newValue);
        });
        new Thread(() -> {
            allPaths =  FXCollections.observableArrayList(docRootService.getAll().stream().map(d -> d.getDocRootPath()).collect(Collectors.toList()));
            Platform.runLater(() -> {
                previousPathsChiceBox.setItems(allPaths);
            });  
            allPaths.forEach(s -> System.out.println("Value :" + s) );
        }).start();
   }
    
    private boolean readAndUpdateFile(String filePath) {
        boolean result = false;
        Path path = Paths.get(filePath);
        try (BufferedReader reader = Files.newBufferedReader(path, FileUtils.ENCODING)) {
            CommandPrompt.write("Updating File := " + filePath, console);
            String strLine;
            StringBuilder fileContent = new StringBuilder();
            while ((strLine = reader.readLine()) != null) {
                if (strLine.toLowerCase().contains(EZAIR_DOCROOT)) { 
                    if (strLine.contains("DocumentRoot")) { //DocumentRoot "C:\Sprint Workspaces\Scrum\ezair_docroot\src\ezair_docroot"
                        String newLine = "DocumentRoot" + SPACE + "\"" + docRootPathTextField.getText() + "\"";
                        fileContent.append(newLine);
                        fileContent.append("\n");
                        CommandPrompt.write("updating := " + strLine + " To := " + newLine, console);
                    } else if (strLine.contains("Directory")) { //<Directory "C:\Sprint Workspaces\Scrum\ezair_docroot\src\ezair_docroot">
                        String newLine = "<Directory" + SPACE + "\"" + docRootPathTextField.getText() + "\">";
                        fileContent.append(newLine);
                        fileContent.append("\n");
                        CommandPrompt.write("updating := " + strLine + " To := " + newLine, console);
                    } 
                } else {
                    // update content as it is
                    fileContent.append(strLine);
                    fileContent.append("\n");
                }
            }
            FileUtils.writeToFile(fileContent.toString(), path);
            result = true;
        } catch (Exception ex) {
         CommandPrompt.write("Exception Ocuured := " + ex.getMessage(), console);
     }   
        return result;
   }

    private boolean validatePaths() {
        boolean result = false;  
        Path configFile = Paths.get(webServerPathTextField.getText()+"\\conf\\httpd.conf");
        Path binFile = Paths.get(webServerPathTextField.getText()+"\\bin\\httpd.exe");
        if(Files.exists(configFile) && Files.exists(binFile)){
            List<String> validDocRootPaths = Arrays.asList("//html","//images","//css","//js");
            long invalidCount = validDocRootPaths.stream().filter(s-> !Files.exists(Paths.get(docRootPathTextField.getText()+s))).count();
            for(String  validPath : validDocRootPaths) {
                if(!Files.exists(Paths.get(docRootPathTextField.getText()+validPath))){
                    CommandPrompt.write("Doc Root path is not valid ", console);
                    return result;
                }
            }
            result = true;
        } else {
            CommandPrompt.write("Webserver path is not valid ", console);
        }
        return result;
    }
}
