/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.presentation.websphere;

import com.chandu0101.ezair.presentation.websphere.docroot.DocRootView;
import com.chandu0101.ezair.presentation.websphere.jvmproperties.JvmPropertiesView;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import com.chandu0101.ezair.presentation.websphere.datasource.DataSourceView;

/**
 *
 * @author chandrasekharkode
 */
public class WebspherePresenter implements Initializable, ChangeListener<TreeItem<String>>  {
    
    @FXML
    private TreeView websphereTools;
    @FXML
    private TextArea websphereConsole;
    @FXML
    private AnchorPane websphereDynamicArea;

    private static WebspherePresenter instance;
    
    private Parent jvmPropertiesView;
    private Parent docRootView;
    private Parent dataSourceView;
    
    public static final String ROOT_ITEM_NAME = "Root";
    public static final String ADMIN_ITEM_NAME = "Admin";
    public static final String JVMPROPERTIES_ITEM_NAME = "JVM Properties";
    public static final String DOCROOT_ITEM_NAME = "Doc Root";
    public static final String DATASOURCE_ITEM_NAME = "Data Source";
    
    public WebspherePresenter() {
        instance = this;
    }
    
    public static WebspherePresenter getInstance() {
        return instance;
    }
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        TreeItem<String> rootItem = new TreeItem<>(ROOT_ITEM_NAME);
        TreeItem<String> adminNode = new TreeItem<>(ADMIN_ITEM_NAME);
        TreeItem<String> jvmProperties = new TreeItem<>(JVMPROPERTIES_ITEM_NAME);
        TreeItem<String> docRoot = new TreeItem<>(DOCROOT_ITEM_NAME);
        TreeItem<String> dataSource = new TreeItem<>(DATASOURCE_ITEM_NAME);
        rootItem.getChildren().add(adminNode);
        rootItem.setExpanded(true);
        adminNode.getChildren().addAll(jvmProperties,docRoot,dataSource);
        websphereTools.setRoot(rootItem);
        websphereTools.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        websphereTools.getSelectionModel().selectedItemProperty().addListener(this);
    }

    public TextArea getWebsphereConsole() {
        return websphereConsole;
    }

    @Override
    public void changed(ObservableValue<? extends TreeItem<String>> ov, TreeItem<String> oldItem, TreeItem<String> newItem) {
        
        switch(newItem.getValue()) {
            
            case JVMPROPERTIES_ITEM_NAME :
                                websphereDynamicArea.getChildren().clear();
                                if(this.jvmPropertiesView == null) {
                                    this.jvmPropertiesView = new JvmPropertiesView().getView();
                                }
                                websphereDynamicArea.getChildren().add(this.jvmPropertiesView);
                                break;
            case DOCROOT_ITEM_NAME: 
                                websphereDynamicArea.getChildren().clear();
                                if(this.docRootView == null) {
                                    this.docRootView = new DocRootView().getView();
                                }
                                websphereDynamicArea.getChildren().add(this.docRootView);
                                break;
            case DATASOURCE_ITEM_NAME:
                               websphereDynamicArea.getChildren().clear();
                               if(this.dataSourceView == null) {
                                   this.dataSourceView = new DataSourceView().getView();
                               }
                               websphereDynamicArea.getChildren().add(dataSourceView);
                              break;                             
            
        }
    }
    
}
