/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chandu0101.ezair.presentation.websphere.datasource;

import com.chandu0101.ezair.constants.Environment;
import com.chandu0101.ezair.presentation.websphere.WebspherePresenter;
import com.chandu0101.ezair.util.CommandPrompt;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;

import static com.chandu0101.ezair.constants.EzairConstants.SPACE;

/**
 *
 * @author chandrasekharkode
 */
public class DataSourcePresenter implements Initializable {

    private TextArea console;

    @FXML
    private TextField profilePathTextField;
    
    @FXML
    private ChoiceBox<Environment> enivChoiceBox;

    @FXML
    private TextField jdbcURLTextField;
    
    @FXML
    private TextField jaasAuthAliastextField;

    @FXML
    private TextField dataSourceTextField;

    private final ObservableList<Environment> envList = FXCollections.observableArrayList(Environment.values());
 
    
    public void modifyDataSource(ActionEvent event) {
          console.clear();
        if(validateInput()) {
            //wsadmin -f modifyDataSource.py dataSourceName jdbcURL jaasName
             String command = "cd" + SPACE + profilePathTextField.getText() +"\\bin" + SPACE + "&&" + SPACE + "wsadmin -f modifyDataSource.py" + SPACE + dataSourceTextField.getText() + SPACE + jdbcURLTextField.getText() +SPACE + jaasAuthAliastextField.getText();
             new Thread(() -> {
                CommandPrompt.execute(command, console);
             }).start();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        this.console = WebspherePresenter.getInstance().getWebsphereConsole();
        enivChoiceBox.setItems(envList);
        enivChoiceBox.valueProperty().addListener((ObservableValue<? extends Environment> ov, Environment oldValue, Environment newvalue) -> {
            switch(newvalue) {
               
                case STAGE : 
                            jaasAuthAliastextField.setText(Environment.STAGE.getName());
                            jdbcURLTextField.setText(Environment.STAGE.getJdbcURL());
                            break;
                case DEV1 : 
                            jaasAuthAliastextField.setText(Environment.DEV1.getName());
                            jdbcURLTextField.setText(Environment.DEV1.getJdbcURL());
                            break;
                case DEV : 
                            jaasAuthAliastextField.setText(Environment.DEV.getName());
                            jdbcURLTextField.setText(Environment.DEV.getJdbcURL());
                            break;     
                
            }
        });

    }

     private boolean validateInput() {
       boolean  result =false;
        if(Files.exists(Paths.get(profilePathTextField.getText()+"\\bin"))) {
             if(Files.exists(Paths.get(profilePathTextField.getText()+"\\bin\\modifyDataSource.py"))) {
                  result =true;
             } else {
                 CommandPrompt.write("modifyDataSource.py file not found in bin directory.", console);
             }
        } else {
            CommandPrompt.write("Profile Path is not valid", console);
        }
       return result;
    }
}
