/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chandu0101.ezair.presentation.websphere.jvmproperties;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import static com.chandu0101.ezair.constants.EzairConstants.*;
import com.chandu0101.ezair.presentation.websphere.WebspherePresenter;
import com.chandu0101.ezair.util.CommandPrompt;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

/**
 *
 * @author chandu0101
 */
public class JvmPropertiesPresenter implements Initializable{

    @FXML
    private TextField profilePathTextField;
    @FXML
    private TextField serverNameTextField;
    @FXML
    private TextField propertyNameTextField;
    @FXML
    private TextField propertyValueTextField;
    
    private TextArea console;

    @FXML
    public void addOrModifyProperty(ActionEvent event) {
        console.clear();
        if(validateInput()) {
             String command = "cd" + SPACE + profilePathTextField.getText() +"\\bin" + SPACE + "&&" + SPACE + "wsadmin -f addJvmProperty.py" + SPACE + serverNameTextField.getText() + SPACE + propertyNameTextField.getText() +SPACE + propertyValueTextField.getText();
             new Thread(() -> {
                CommandPrompt.execute(command, console);
                CommandPrompt.write("Completed!", console);
             }).start();
        }
       
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.console = WebspherePresenter.getInstance().getWebsphereConsole();
    }

    private boolean validateInput() {
       boolean  result =false;
        if(Files.exists(Paths.get(profilePathTextField.getText()+"\\bin"))) {
             if(Files.exists(Paths.get(profilePathTextField.getText()+"\\bin\\addJvmProperty.py"))) {
                  result =true;
             } else {
                 CommandPrompt.write("addJvmProperty.py file not found in bin directory.", console);
             }
        } else {
            CommandPrompt.write("Profile Path is not valid", console);
        }
       return result;
    }
}
