/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.presentation.base;

import com.chandu0101.ezair.presentation.Token.TokenView;
import com.chandu0101.ezair.presentation.about.AboutView;
import com.chandu0101.ezair.presentation.svn.SvnView;
import com.chandu0101.ezair.presentation.websphere.WebsphereView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 * @author chandrasekharkode
 */
public class BasePresenter implements Initializable{
    
   
    @FXML
    private AnchorPane webSphereUtils;
    @FXML
    private AnchorPane tokenArea;
    @FXML
    private  AnchorPane svnBase;

    @FXML
    private Button aboutButton;
    private PopOver popOver;
    private static BasePresenter instance;
    private double targetX;
    private double targetY;
    private Stage stageRef;
    
    public BasePresenter() {
        instance = this;
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        AboutView aboutView = new AboutView();
        popOver = new PopOver(aboutView.getView());
        popOver.setDetachedTitle("About");
        WebsphereView websphereView = new WebsphereView();
        webSphereUtils.getChildren().add(websphereView.getView());
        TokenView tokenView = new TokenView();
        tokenArea.getChildren().addAll(tokenView.getView());
        SvnView svnView = new SvnView();
        svnBase.getChildren().addAll(svnView.getView());
    }

    @FXML
    public void showAboutScreen(ActionEvent event) {
        targetX = stageRef.getX() + 250;
        targetY = stageRef.getY() + 100;
        popOver.show(aboutButton, targetX,targetY);
        popOver.setDetached(true);
    }


    public void setStageRef(final Stage stageRef) {
        this.stageRef = stageRef;
    }
}
