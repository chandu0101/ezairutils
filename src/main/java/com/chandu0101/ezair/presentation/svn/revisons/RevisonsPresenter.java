package com.chandu0101.ezair.presentation.svn.revisons;

import com.chandu0101.ezair.constants.Project;
import com.chandu0101.ezair.constants.Repo;
import com.chandu0101.ezair.presentation.svn.SvnPresenter;
import com.chandu0101.ezair.presentation.svn.login.LoginPresenter;
import com.chandu0101.ezair.util.CommandPrompt;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.io.SVNRepository;

import java.net.URL;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.chandu0101.ezair.constants.EzairConstants.US_DATE_FORMAT;
import static com.chandu0101.ezair.util.DateUtil.*;

/**
 * Created by chandrasekharkode on 1/17/14.
 */
public class RevisonsPresenter implements Initializable {


    @FXML
    private RadioButton commentRadioButton;

    @FXML
    private TextField commentTextField;

    @FXML
    private RadioButton datesRadioButton;

    @FXML
    private TextField fromDateTextField;

    @FXML
    private ComboBox<Project> projectComboBox;

    @FXML
    private ComboBox<Repo> repoComboBox;

    @FXML
    private ToggleGroup searchBy;

    @FXML
    private TextField toDateTextField;

    private TextArea svnConsle;

    private static final ObservableList<Project> projectList = FXCollections.observableArrayList(Project.values());
    private static final ObservableList<Repo> repoList = FXCollections.observableArrayList(Repo.values());
    private static final int START_REVISION = 0;
    private static final int END_REVISION = -1; // HEAD (the latest) revision
    private SVNRepository svnRepository;

    @FXML
    void handleSearch(ActionEvent event) {

        svnConsle.clear();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        switch(repoComboBox.getValue()) {

            case SCRUM:
                if(projectComboBox.getValue() == Project.ALL) {
                    executorService.submit(() -> {
                        searchRevisions(Project.EZAIR_BOOK.getScrumPath(), Project.EZAIR_BOOK);
                        searchRevisions(Project.EZAIR_ADMIN.getScrumPath(), Project.EZAIR_ADMIN);
                        searchRevisions(Project.EZAIR_BIZZMANAGER.getScrumPath(), Project.EZAIR_BIZZMANAGER);
                        searchRevisions(Project.EZAIR_COMMON_DATA_ACCESS.getScrumPath(), Project.EZAIR_COMMON_DATA_ACCESS);
                        searchRevisions(Project.EZAIR_DOCROOT.getScrumPath(), Project.EZAIR_DOCROOT);
                        searchRevisions(Project.EZAIR_SERVICE.getScrumPath(), Project.EZAIR_SERVICE);
                    });

                } else {
                    executorService.submit(() -> searchRevisions(projectComboBox.getValue().getScrumPath(), projectComboBox.getValue()));
                }
                break;

            case TRUNK:
                if(projectComboBox.getValue() == Project.ALL) {
                    executorService.submit(() -> {
                        searchRevisions(Project.EZAIR_BOOK.getTrunkPath(),  Project.EZAIR_BOOK);
                        searchRevisions(Project.EZAIR_ADMIN.getTrunkPath(),  Project.EZAIR_ADMIN);
                        searchRevisions(Project.EZAIR_BIZZMANAGER.getTrunkPath(),  Project.EZAIR_BIZZMANAGER);
                        searchRevisions(Project.EZAIR_COMMON_DATA_ACCESS.getTrunkPath(),  Project.EZAIR_COMMON_DATA_ACCESS);
                        searchRevisions(Project.EZAIR_DOCROOT.getTrunkPath(),  Project.EZAIR_DOCROOT);
                        searchRevisions(Project.EZAIR_SERVICE.getTrunkPath(),  Project.EZAIR_SERVICE);
                    });


                } else {
                    executorService.submit(() -> searchRevisions(projectComboBox.getValue().getTrunkPath(), projectComboBox.getValue())) ;
                }
                break;

            case STAGE:
                if(projectComboBox.getValue() == Project.ALL) {
                    executorService.submit(() -> {
                        searchRevisions(Project.EZAIR_BOOK.getStagePath(),Project.EZAIR_BOOK);
                        searchRevisions(Project.EZAIR_ADMIN.getStagePath(), Project.EZAIR_ADMIN);
                        searchRevisions(Project.EZAIR_BIZZMANAGER.getStagePath(),Project.EZAIR_BIZZMANAGER);
                        searchRevisions(Project.EZAIR_COMMON_DATA_ACCESS.getStagePath(),Project.EZAIR_COMMON_DATA_ACCESS);
                        searchRevisions(Project.EZAIR_DOCROOT.getStagePath() ,Project.EZAIR_DOCROOT);
                        searchRevisions(Project.EZAIR_SERVICE.getStagePath(),Project.EZAIR_SERVICE);
                    });
                } else {
                    executorService.submit(() -> searchRevisions(projectComboBox.getValue().getStagePath(),   projectComboBox.getValue()));
                }
                break;

            case PROD:
                if(projectComboBox.getValue() == Project.ALL) {
                    executorService.submit(() -> {
                        searchRevisions(Project.EZAIR_BOOK.getProdPath(), Project.EZAIR_BOOK);
                        searchRevisions(Project.EZAIR_ADMIN.getProdPath(), Project.EZAIR_ADMIN);
                        searchRevisions(Project.EZAIR_BIZZMANAGER.getProdPath(), Project.EZAIR_BIZZMANAGER);
                        searchRevisions(Project.EZAIR_COMMON_DATA_ACCESS.getProdPath(),  Project.EZAIR_COMMON_DATA_ACCESS);
                        searchRevisions(Project.EZAIR_DOCROOT.getProdPath(), Project.EZAIR_DOCROOT);
                        searchRevisions(Project.EZAIR_SERVICE.getProdPath(), Project.EZAIR_SERVICE);
                    });

                } else {
                    executorService.submit(() -> searchRevisions(projectComboBox.getValue().getProdPath(),   projectComboBox.getValue()));
                }
                break;

        }

        executorService.shutdown();

    }

    private void searchRevisions(String repoPath,Project project) {

       if(commentRadioButton.isSelected()) {
           printRevisionNumbers(getRevisionsforProjectWithCooment(repoPath,commentTextField.getText()),project);
       } else {
           printRevisionNumbers(getRevisionsforProjectWithDates(repoPath, convertStringToLocalDate(fromDateTextField.getText(), US_DATE_FORMAT), convertStringToLocalDate(toDateTextField.getText(), US_DATE_FORMAT)),project);
       }
    }



    private List<Long> getRevisionsforProjectWithCooment(String repoPath,String comment) {
        List<Long> revisions = null;
        try {
            svnRepository.setLocation(SVNURL.parseURIEncoded(repoPath),true);
            Collection<SVNLogEntry> logs = svnRepository.log(new String[] {""},null,START_REVISION,END_REVISION,true,true);
            revisions = logs.stream().filter(log -> log.getMessage().toLowerCase().contains(comment.toLowerCase()))
                         .map(log -> log.getRevision())
                         .collect(Collectors.toList());
        } catch (SVNException e) {
            e.printStackTrace();
            CommandPrompt.write("Exception occured while fetching revision numbers : " + e.getMessage(),svnConsle);
        }

        return revisions;
    }

    private List<Long>  getRevisionsforProjectWithDates(String repoPath,LocalDate startDate,LocalDate endDate) {
        List<Long> revisions = null;
        try {
            svnRepository.setLocation(SVNURL.parseURIEncoded(repoPath),true);
            Collection<SVNLogEntry> logs = svnRepository.log(new String[] {""},null,START_REVISION,END_REVISION,true,true);
            revisions = logs.stream().filter(log -> isBetween(getLocalDate(log.getDate()), startDate, endDate))
                    .map(log -> log.getRevision())
                    .collect(Collectors.toList());
        } catch (SVNException e) {
            e.printStackTrace();
            CommandPrompt.write("Exception occured while fetching revision numbers : " + e.getMessage(),svnConsle);
        }
        return revisions;

    }

    private void printRevisionNumbers(List<Long> revisions,Project project) {
        CommandPrompt.write("Fetching Revision Numbers for Project: "+project.toString() ,svnConsle);
        StringBuffer commaSeparatedRevisions = new StringBuffer();
        if(revisions != null) {
            revisions.forEach(revision -> commaSeparatedRevisions.append(revision+","));
        }
        CommandPrompt.write(project.toString()+" :: " +commaSeparatedRevisions.toString(),svnConsle);
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        projectComboBox.setItems(projectList);
        projectComboBox.getSelectionModel().selectFirst();
        repoComboBox.setItems(repoList);
        repoComboBox.getSelectionModel().selectFirst();
        this.svnRepository = LoginPresenter.getINSTANCE().getSvnRepository();
        this.svnConsle = SvnPresenter.getINSTANCE().getSvnConsole();
    }
}
