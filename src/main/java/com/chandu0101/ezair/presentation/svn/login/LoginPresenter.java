/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.presentation.svn.login;

import com.chandu0101.ezair.presentation.svn.SvnPresenter;
import com.chandu0101.ezair.util.CommandPrompt;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import java.net.URL;
import java.util.ResourceBundle;

import static com.chandu0101.ezair.constants.EzairConstants.SVN_EZAIR_BOOK_PATH;
import static com.chandu0101.ezair.constants.EzairConstants.SVN_TRUNK_PATH;

/**
 *
 * @author pc07598
 */
public class LoginPresenter implements Initializable {

    @FXML
    private PasswordField passwordTextField;

    @FXML
    private TextField usernameTextField;

    @FXML
    private Button loginButton;

    private SVNRepository svnRepository;

    private TextArea svnConsole;

    private static LoginPresenter INSTANCE ;

    public LoginPresenter() {
        INSTANCE = this;
    }

    public static LoginPresenter getINSTANCE() {
        return INSTANCE;
    }

    public SVNRepository getSvnRepository() {
        return svnRepository;
    }

    @FXML
    void handleLogin(ActionEvent event) {
        try {
            svnConsole.clear();
            CommandPrompt.write("Connecting to SVN ...",svnConsole);
            /*
               * For using over http:// and https://
            */
            DAVRepositoryFactory.setup();
            svnRepository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(SVN_EZAIR_BOOK_PATH+SVN_TRUNK_PATH));
            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(usernameTextField.getText(), passwordTextField.getText());
            svnRepository.setAuthenticationManager(authManager);
            svnRepository.testConnection();
            CommandPrompt.write("Successfully Connected",svnConsole);
            SvnPresenter.getINSTANCE().getSvnTools().getSelectionModel().select(2);
        } catch (SVNException e) {
            e.printStackTrace();
            CommandPrompt.write("Exception while connection to "+SVN_EZAIR_BOOK_PATH + ":= " + e.getMessage(),svnConsole);
        }
    }



    @Override
    public void initialize(URL url, ResourceBundle rb) {

        svnConsole = SvnPresenter.getINSTANCE().getSvnConsole();
        BooleanProperty usernameEntered = new SimpleBooleanProperty();
        usernameEntered.bind(usernameTextField.textProperty().length().greaterThan(6));
        BooleanProperty passwordEntered = new SimpleBooleanProperty();
        passwordEntered.bind(passwordTextField.textProperty().length().greaterThan(6));
        loginButton.disableProperty().bind(usernameEntered.and(passwordEntered).not());

    }
    
}
