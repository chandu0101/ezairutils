package com.chandu0101.ezair.presentation.svn;

import com.chandu0101.ezair.presentation.svn.login.LoginView;
import com.chandu0101.ezair.presentation.svn.revisons.RevisonsView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by chandrasekharkode on 1/17/14.
 */
public class SvnPresenter implements Initializable,ChangeListener<TreeItem<String>> {

    @FXML
    private TextArea svnConsole;

    @FXML
    private AnchorPane svnDynamicArea;

    @FXML
    private TreeView<String> svnTools;

    private Parent revisionsView;
    private Parent loginView;

    private static final String ROOT_ITEM_NAME = "SVN";
    private static final String REVISION_ITEM_NAME = "Revisions";
    private static final String LOGIN_ITEM_NAME = "Login";

    private static SvnPresenter INSTANCE;
    public SvnPresenter() {
        INSTANCE = this;
    }

    public TreeView<String> getSvnTools() {
        return svnTools;
    }

    public static SvnPresenter getINSTANCE() {
        return INSTANCE;
    }

    public TextArea getSvnConsole() {
        return svnConsole;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

       TreeItem<String> rootItem = new TreeItem<>(ROOT_ITEM_NAME);
       TreeItem<String> revisonsItem = new TreeItem<>(REVISION_ITEM_NAME);
       TreeItem<String> loginItem = new TreeItem<>(LOGIN_ITEM_NAME);
       rootItem.getChildren().addAll(loginItem,revisonsItem);
       rootItem.setExpanded(true);
       svnTools.setRoot(rootItem);
       svnTools.getSelectionModel().selectedItemProperty().addListener(this);
       svnTools.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
       svnTools.getSelectionModel().select(loginItem);
    }

    @Override
    public void changed(ObservableValue<? extends TreeItem<String>> observableValue, TreeItem<String> oldItem, TreeItem<String> newItem) {

        switch (newItem.getValue()) {

            case REVISION_ITEM_NAME :
                                      if(revisionsView == null) {
                                          revisionsView = new RevisonsView().getView();
                                      }
                                       svnDynamicArea.getChildren().clear();
                                       svnDynamicArea.getChildren().add(revisionsView);
                                       break;
             case LOGIN_ITEM_NAME :
                                      if(loginView == null) {
                                          loginView = new LoginView().getView();
                                      }
                                       svnDynamicArea.getChildren().clear();
                                       svnDynamicArea.getChildren().add(loginView);
                                       break;    

        }

    }
}
