package com.chandu0101.ezair.constants;

/**
 * Created by chandrasekharkode on 12/8/13.
 */
public enum PolarEnvironment {
    RES("dev link"),RE1("dev1 link"),RE3("dev link"),PCQ("stage link"),HAQ("haq link"),LOCAL("local link");

    private String link;
    private PolarEnvironment(String link) {
       this.link = link;
    }

    public String getLink() {
        return link;
    }


}
