/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.constants;

/**
 *
 * @author chandrasekharkode
 */
public enum Environment {
    STAGE("stage","jdbc:oracle:thin:@INTERSTG:1596:INTERSTG"),DEV1("dev1","jdbc:oracle:thin:@INTERDEV:1535:INTERDEV"),DEV("dev","jdbc:oracle:thin:@INTERDEV:1535:INTERDEV");
    private String name;
    private String jdbcURL;
    private Environment(String name,String jdbcURL) {
        this.name = name;
        this.jdbcURL = jdbcURL;
    }
    
    public String getName() {
        return name;
    }
    
    public String getJdbcURL() {
        return jdbcURL;
    }
}
