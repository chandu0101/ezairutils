/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.chandu0101.ezair.constants;

/**
 *
 * @author chandu0101
 */
public final class EzairConstants {
    
   public static final String SPACE = " ";
   public static final String NEW_LINE = "\n";
   public static final String EZAIR_DOCROOT = "ezair_docroot";
   public static final String SUCCESS = "success";
   public static final String ERROR = "error";
   public static final String SVN_BASE_PATH = "https://appdevsrc.cruises.princess.com/svn/Web/";
   public static final String SVN_EZAIR_BOOK_PATH = SVN_BASE_PATH+"ezair_book/";
   public static final String SVN_EZAIR_ADMIN_PATH = SVN_BASE_PATH+"ezair_admin/";
   public static final String SVN_EZAIR_BIZZMANAGER_PATH = SVN_BASE_PATH+"ezair_bizzmanager/";
   public static final String SVN_EZAIR_COMMON_DATA_ACCESS_PATH = SVN_BASE_PATH+"ezair_common_data_access/";
   public static final String SVN_EZAIR_DOCROOT_PATH = SVN_BASE_PATH+"ezair_docroot/";
   public static final String SVN_EZAIR_SERVICE_PATH = SVN_BASE_PATH+"ezair_service/";
   public static final String SVN_TRUNK_PATH = "trunk";
   public static final String SVN_SCRUM_PATH = "branches/scrum";
   public static final String SVN_STAGE_PATH = "build/stage";
   public static final String SVN_PROD_PATH = "build/prod";
   public static final String US_DATE_FORMAT = "MM/dd/yyyy";

}
