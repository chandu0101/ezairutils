package com.chandu0101.ezair.constants;

/**
 * Created by chandrasekharkode on 1/18/14.
 */
import static com.chandu0101.ezair.constants.EzairConstants.*;

public enum Project {
    ALL("","","","",""),
    EZAIR_BOOK(SVN_EZAIR_BOOK_PATH,SVN_EZAIR_BOOK_PATH+SVN_TRUNK_PATH,SVN_EZAIR_BOOK_PATH+SVN_SCRUM_PATH,SVN_EZAIR_BOOK_PATH+SVN_STAGE_PATH,SVN_EZAIR_BOOK_PATH+SVN_PROD_PATH),
    EZAIR_ADMIN(SVN_EZAIR_ADMIN_PATH,SVN_EZAIR_ADMIN_PATH+SVN_TRUNK_PATH,SVN_EZAIR_ADMIN_PATH+SVN_SCRUM_PATH,SVN_EZAIR_ADMIN_PATH+SVN_STAGE_PATH,SVN_EZAIR_ADMIN_PATH+SVN_PROD_PATH),
    EZAIR_BIZZMANAGER(SVN_EZAIR_BIZZMANAGER_PATH,SVN_EZAIR_BIZZMANAGER_PATH+SVN_TRUNK_PATH,SVN_EZAIR_BIZZMANAGER_PATH+SVN_SCRUM_PATH,SVN_EZAIR_BIZZMANAGER_PATH+SVN_STAGE_PATH,SVN_EZAIR_BIZZMANAGER_PATH+SVN_PROD_PATH),
    EZAIR_COMMON_DATA_ACCESS(SVN_EZAIR_COMMON_DATA_ACCESS_PATH,SVN_EZAIR_COMMON_DATA_ACCESS_PATH+SVN_TRUNK_PATH,SVN_EZAIR_COMMON_DATA_ACCESS_PATH+SVN_SCRUM_PATH,SVN_EZAIR_COMMON_DATA_ACCESS_PATH+SVN_STAGE_PATH,SVN_EZAIR_COMMON_DATA_ACCESS_PATH+SVN_PROD_PATH),
    EZAIR_DOCROOT(SVN_EZAIR_DOCROOT_PATH,SVN_EZAIR_DOCROOT_PATH+SVN_TRUNK_PATH,SVN_EZAIR_DOCROOT_PATH+SVN_SCRUM_PATH,SVN_EZAIR_DOCROOT_PATH+SVN_STAGE_PATH,SVN_EZAIR_DOCROOT_PATH+SVN_PROD_PATH),
    EZAIR_SERVICE(SVN_EZAIR_SERVICE_PATH,SVN_EZAIR_SERVICE_PATH+SVN_TRUNK_PATH,SVN_EZAIR_SERVICE_PATH+SVN_SCRUM_PATH,SVN_EZAIR_SERVICE_PATH+SVN_STAGE_PATH,SVN_EZAIR_SERVICE_PATH+SVN_PROD_PATH);

    private String basePath;
    private String trunkPath;
    private String scrumPath;
    private String stagePath;
    private String prodPath;

    private Project(String basePath, String trunkPath, String scrumPath, String stagePath, String prodPath) {
        this.basePath = basePath;
        this.trunkPath = trunkPath;
        this.scrumPath = scrumPath;
        this.stagePath = stagePath;
        this.prodPath = prodPath;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getTrunkPath() {
        return trunkPath;
    }

    public String getScrumPath() {
        return scrumPath;
    }

    public String getStagePath() {
        return stagePath;
    }

    public String getProdPath() {
        return prodPath;
    }
}
