package com.chandu0101.ezair.util;

import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by chandrasekharkode on 1/25/14.
 */
public class DateUtilTest {
    @Test
    public void testGetLocalDate() throws Exception {
       assertEquals(LocalDate.now(), DateUtil.getLocalDate(new Date()));
    }

    @Test
    public void testIsBetween() throws Exception {
       LocalDate today = LocalDate.now();
      assertTrue(DateUtil.isBetween(today,today.minusDays(1),today.plusDays(1)));
    }

    @Test
    public void testConvertStringToLocalDate() throws Exception {
        LocalDate today = LocalDate.now();
       assertEquals(today, DateUtil.convertStringToLocalDate(today.format(DateTimeFormatter.ISO_LOCAL_DATE), "yyyy-MM-dd"));
    }
}
